module ApplicationHelper
  def full_title(page_title: '')
    if page_title.blank?
      CustomConstants::BASE_TITLE
    else
      "#{page_title} - #{CustomConstants::BASE_TITLE}"
    end
  end
end
