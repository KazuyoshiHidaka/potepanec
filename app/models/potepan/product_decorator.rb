module Potepan::ProductDecorator
  def related_products(max: CustomConstants::MAX_RELATED_PRODUCT_COUNT)
    Spree::Product.in_taxons(taxons).where.not(id: id).distinct.limit(max)
  end

  Spree::Product.prepend self
end
