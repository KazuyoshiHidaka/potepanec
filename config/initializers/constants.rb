# frozen_string_literal: true

module CustomConstants
  BASE_TITLE = 'BIGBAG Store'
  MAX_RELATED_PRODUCT_COUNT = 4
end
