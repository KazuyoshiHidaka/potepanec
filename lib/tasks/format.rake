namespace :format do
  desc "erbファイルをフォーマットする"
  task erb: :environment do
    files_excluded =
      Dir.glob('app/views/layouts/mailer.*.erb') + Dir.glob('app/views/potepan/sample/[^index]*')
    files = Dir.glob("app/views/**/*.erb") - files_excluded

    system "bundle exec htmlbeautifier #{files.join(' ')}"
  end
end
