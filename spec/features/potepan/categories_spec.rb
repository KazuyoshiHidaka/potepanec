require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  let(:taxon) do
    create(:taxon) do |taxon|
      root = taxon.taxonomy.root
      taxon.move_to_child_of(root)

      create(:shipping_category)
      taxon.products.create(attributes_for(:product))
    end
  end
  let(:product) { taxon.products.first }

  feature "#show" do
    before do
      visit potepan_category_path(taxon.id)
    end

    scenario "タイトルが 'カテゴリー名 - #{CustomConstants::BASE_TITLE}'であること" do
      expect(page).to have_title "#{taxon.name} - #{CustomConstants::BASE_TITLE}"
    end

    feature "left sidebar" do
      scenario "カテゴリー群の名前が表示されている" do
        expect(page).to have_content taxon.taxonomy.name
      end

      scenario "カテゴリー名(商品数)リンク が正常に機能する" do
        click_link "#{taxon.name} (#{taxon.products.length})", href: potepan_category_path(taxon.id)
        expect(current_path).to eq potepan_category_path(taxon.id)
      end
    end

    feature "product grid" do
      context "Productがカテゴリーに属する場合" do
        scenario "情報が表示されている" do
          aggregate_failures do
            expect(page).to have_content product.name
            expect(page).to have_content product.display_price
          end
        end
      end

      context "Productがカテゴリーに属さない場合" do
        let(:product_not_in_category) do
          create(:product, price: product.price + 1)
        end

        scenario "情報が表示されない" do
          aggregate_failures do
            expect(page).to have_no_content product_not_in_category.name
            expect(page).to have_no_content product_not_in_category.display_price
          end
        end
      end

      scenario "Productをクリックすると、Product詳細画面が表示される" do
        click_link product.name
        expect(current_path).to eq potepan_product_path(product.id)
      end
    end
  end
end
