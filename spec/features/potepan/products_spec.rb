require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  let(:product) { create(:product, taxons: [taxon]) }
  let(:taxon) { create(:taxon) }

  feature "#show" do
    before do
      visit potepan_product_path(product.id)
    end

    scenario "タイトルが 'Single Product - #{CustomConstants::BASE_TITLE}'であること" do
      expect(page).to have_title "Single Product - #{CustomConstants::BASE_TITLE}"
    end

    scenario "期待するProductインスタンスの情報が表示される" do
      aggregate_failures do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
        expect(page).to have_content product.description
      end
    end

    context "Productにカテゴリーが登録されている場合" do
      scenario "カテゴリーページへ戻るリンクが正常に機能する" do
        click_link "一覧ページへ戻る", href: potepan_category_path(taxon.id)
        expect(current_path).to eq potepan_category_path(taxon.id)
      end
    end

    context "Productにカテゴリーが登録されていない場合" do
      let(:product_with_no_taxons) { create(:product) }

      scenario "カテゴリーページへ戻るリンクが表示されない" do
        visit potepan_product_path(product_with_no_taxons.id)
        expect(page).to have_no_link "一覧ページへ戻る"
      end
    end

    feature "関連商品の表示" do
      let!(:other_related_product) do
        create(:product, taxons: [taxon])
      end

      scenario "関連商品が表示される" do
        visit potepan_product_path(product.id)
        aggregate_failures do
          expect(page).to have_content other_related_product.name
          expect(page).to have_content other_related_product.display_price
          click_link other_related_product.name
          expect(current_path).to eq potepan_product_path(other_related_product.id)
        end
      end

      context "関連商品が#{CustomConstants::MAX_RELATED_PRODUCT_COUNT}個より多い場合" do
        let!(:other_related_products) do
          create_list(:product, CustomConstants::MAX_RELATED_PRODUCT_COUNT + 1, taxons: [taxon])
        end
        let(:not_included_related_products) do
          other_related_products - product.related_products.to_a
        end

        it "#{CustomConstants::MAX_RELATED_PRODUCT_COUNT}個まで表示される" do
          visit potepan_product_path(product.id)

          aggregate_failures do
            product.related_products.each do |related_product|
              expect(page).to have_content related_product.name
            end

            not_included_related_products.each do |not_included_related_product|
              expect(page).to have_no_content not_included_related_product.name
            end
          end
        end
      end
    end
  end
end
