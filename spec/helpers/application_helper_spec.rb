require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title" do
    base_title = CustomConstants::BASE_TITLE
    subject { helper.full_title(page_title: page_title) }

    context "引数にページタイトルを与えた場合" do
      let(:page_title) { "example" }

      it "'ページタイトル - #{base_title}' を返す" do
        is_expected.to eq "#{page_title} - #{base_title}"
      end
    end

    context "引数がnilの場合" do
      let(:page_title) { nil }

      it "'#{base_title}' を返す" do
        is_expected.to eq base_title
      end
    end

    context "引数が空文字の場合" do
      let(:page_title) { '' }

      it "'#{base_title}' を返す" do
        is_expected.to eq base_title
      end
    end

    context "引数を与えない場合" do
      it "'#{base_title}' を返す" do
        expect(helper.full_title).to eq base_title
      end
    end
  end
end
