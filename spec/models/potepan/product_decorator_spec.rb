require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  describe "#related_products" do
    subject { product.related_products }

    let(:product) do
      create(:product, taxons: create_list(:taxon, 2, products: related_products))
    end
    let(:related_products) do
      create_list(:product, CustomConstants::MAX_RELATED_PRODUCT_COUNT)
    end

    it "レシーバーのProductインスタンスが関連商品に含まれていない" do
      expect(subject.include?(product)).to be false
    end

    it "重複するProductインスタンスが含まれていない" do
      is_expected.to eq subject.distinct
    end

    describe "関連商品の最大取得数" do
      subject { product.related_products(max: max_length).length }

      let(:related_products) do
        create_list(:product, CustomConstants::MAX_RELATED_PRODUCT_COUNT + 1)
      end

      context "引数を与えなかった場合" do
        it "デフォルトの最大取得数で取得数を制限できている" do
          expect(product.related_products.length).to be CustomConstants::MAX_RELATED_PRODUCT_COUNT
        end
      end

      context "引数に整数を指定した場合" do
        let(:max_length) { CustomConstants::MAX_RELATED_PRODUCT_COUNT - 1 }

        it "引数の値が最大取得数に適用される" do
          is_expected.to be max_length
        end
      end
    end

    context "非関連商品がある場合" do
      let!(:not_related_product) { create(:product) }

      it "非関連商品は取得できない" do
        expect(subject.include?(not_related_product)).to be false
      end
    end

    it "期待する関連商品が取得できている" do
      is_expected.to match_array(related_products)
    end
  end
end
