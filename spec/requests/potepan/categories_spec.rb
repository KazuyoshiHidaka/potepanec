require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  let(:taxon) { create(:taxon) }

  describe "GET /potepan/categories/:id" do
    it "200レスポンスを返す" do
      get potepan_category_path(taxon.id)
      expect(response).to have_http_status(200)
    end
  end
end
