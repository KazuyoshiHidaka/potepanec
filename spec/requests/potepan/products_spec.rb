require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  let(:product) do
    create(:product) do |product|
      product.taxons.create(attributes_for(:taxon))
    end
  end

  describe "GET /potepan/products/:id" do
    before { get potepan_product_path(product.id) }

    it "200レスポンスを返す" do
      expect(response).to have_http_status(200)
    end
  end
end
